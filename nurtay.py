import time
import random


def retry(*exceptions, attempts, delay, backoff):
    def decorator(func):
        def wrapper(*args, **kwargs):
            nonlocal delay
            if attempts <= 0 or delay <= 0 or backoff <= 0:
                raise ValueError('Отрицательные значения не допустимы')
            
            for i in range(attempts):
                try: 
                    return func(*args, **kwargs)
                except exceptions as e:
                    print(f'Ошибка {e}')
                    if i != attempts - 1:
                        time.sleep(delay)
                    delay += backoff

            raise Exception("Попытки завершены. Функция не выполнена успешно.")
        return wrapper
    return decorator

@retry(
    ValueError, # на какие ошибки повторно запускать функцию
    ZeroDivisionError,
    attempts=3, # сколько всего попыток
    delay=3, # какая изначальная задержка ( после первой попытки )
    backoff=2, # насколько увеличивать задержку между попытками 
)
def my_function():
    num = random.randint(0, 1)
    print(f'1 / {num} = {1 / num}')

my_function()



# @retry(
#     ValueError,
#     StopIteration, # на какие ошибки повторно запускать функцию
#     attemtps=3, # сколько всего попыток
#     delay=3, # какая изначальная задержка ( после первой попытки )
#     backoff=2, # насколько увеличивать задержку между попытками       
# )
# def tests(i=2):
#     if i % 2 == 0:
#         raise ValueError
#     return i

# tests()


# @retry(
#     SystemExit,
#     attemtps=10,
#     delay=0.5,
#     backoff=1,
# )
# def print_logs():
#     pass




